from django.apps import AppConfig


class PetManagamentConfig(AppConfig):
    name = 'pet_managament'
