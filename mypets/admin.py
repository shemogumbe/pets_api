from django.contrib import admin

from .models import User, Dog, Cat


class DogAdmin(admin.ModelAdmin):
    list_display = ('name', 'birth_date')

    def get_queryset(self, request):
        qs = super(DogAdmin, self).get_queryset(request)
        # if request.user.is_superuser:
        #     return qs
        return qs.filter(owner=request.user)


class CatAdmin(admin.ModelAdmin):
    list_display = ('name', 'birth_date')

    def get_queryset(self, request):
        qs = super(CatAdmin, self).get_queryset(request)
            # if request.user.is_superuser:
            #     return qs
        return qs.filter(owner=request.user)


admin.site.register(Dog, DogAdmin)
admin.site.register(Cat, CatAdmin)
