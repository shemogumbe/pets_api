import json

from django.shortcuts import render

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


from .models import Dog, Cat, User
from .serializers import DogSerializer, CatSerializer


class DogListView(generics.ListCreateAPIView):
    # permission_classes = (IsAuthenticated,)

    queryset = Dog.objects.all()
    serializer_class = DogSerializer

    def post(self, request, *args, **kwargs):
        request.data['owner'] = request.user
        Dog.objects.create(**request.data)
        dog_added_msg = {'success': 'Your Dog was added'}
        return Response(dog_added_msg)


    def list(self, request):

        ''' Get only dogs belonging to a specific user '''

        dogs = Dog.objects.filter(
            owner=request.user)
        my_dogs = []
        for dog in dogs:
            the_dog = {
               'name': dog.name,
               'id':dog.id,
               'birth_date': dog.birth_date
            }
            my_dogs.append(the_dog)

        return Response(my_dogs)




class DogDetailView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (IsAuthenticated,)

    queryset = Dog.objects.all()
    serializer_class = DogSerializer

    
    def put(self, request, pk):
        try:
            if not request.data:
                return Response({'Error': 'Invalid data'})
            dog = Dog.objects.get(id=pk, owner=request.user)
            if 'name' in request.data:
                dog.name = request.data['name']
            if 'birth_date' in request.data:
                dog.birth_date = request.data['birth_date']
                dog.save()
            dog.save()
            return Response({'success': 'Updated'})
        except Exception as e:
            return Response({'error': 'Does not exist'})

    def delete(self, request, pk):
        try:
            dog = Dog.objects.get(id=pk, owner=request.user)
            dog.delete()
            return Response({'success': 'Dog deleted'})
        except Exception as e:
            return Response(
                {'error': 'Does not exist'})







class CatListView(generics.ListCreateAPIView):
    # permission_classes = (IsAuthenticated,)
    queryset = Cat.objects.all()
    serializer_class = CatSerializer

    def post(self, request, *args, **kwargs):
        request.data['owner'] = request.user
        Cat.objects.create(**request.data)
        cat_added_msg = {'success': 'Your Cat was added'}
        return Response(cat_added_msg)


    def list(self, request):

        ''' Get only dogs belonging to a specific user '''

        cats = Cat.objects.filter(
            owner=request.user)

        my_cats = []
        for cat in cats:
            the_cat = {
               'name': cat.name,
               'id':cat.id,
               'birth_date': cat.birth_date
            }
            my_cats.append(the_cat)
        return Response(my_cats)


class CatDetailView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (IsAuthenticated,)


    queryset = Cat.objects.all()
    serializer_class = CatSerializer

    def put(self, request, pk):
        try:
            if not request.data:
                return Response({'error': 'Invalid data'})
            cat = Cat.objects.get(id=pk, owner=request.user)
            if 'name' in request.data:
                cat.name = request.data['name']
            if 'birth_date' in request.data:
                cat.birth_date = request.data['birth_date']
                cat.save()
            cat.save()
            return Response({'success': 'Updated'})
        except Exception as e:
            return Response({'error': 'Cat Does not exist'})

    def delete(self, request, pk):
        try:
            cat = Cat.objects.get(id=pk, owner=request.user)
            cat.delete()
            return Response({'success': 'Cat deleted'})
        except Exception as e:
            return Response(
                {'error': 'Cat Does not exist'})


