from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models

# Create your models here.
class UserManger(BaseUserManager):

    ''' manager for Custom user model '''

    def create_user(self, username, password=None, **other_fields):
        if not username:
            return 'User must have a valid username'

        user = self.model(username=username)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **other_fields):
        if not username:
            return 'User must have a valid username'

        user = self.model(username=username)

        user.set_password(password)
        user.is_admin = True
        is_staff = True
        is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractUser):

    """  Custom user model for uuid pk,
        company and creation date  """

    user_type = models.CharField(max_length=10, default="")
    password = models.CharField(max_length=254)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password']

    objects = UserManger()

    def delete(self):

        ''' sets user to inactive upon delete'''

        self.is_active = False
        return super(AbstractUser, self).delete()


class Dog(models.Model):
    name = models.CharField(max_length=30)
    owner = models.ForeignKey(
        'User', related_name='dog_owner',
        on_delete=models.CASCADE)
    birth_date = models.DateField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'dogs'


class Cat(models.Model):
    name = models.CharField(max_length=30)
    owner = models.ForeignKey(
        'User', related_name='cat_owner',
        on_delete=models.CASCADE)
    birth_date = models.DateField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cats'

