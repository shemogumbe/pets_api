from rest_framework import serializers

from .models import Dog, Cat


class DogSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    owner = serializers.CharField(source='owner.id', read_only=True)


    class Meta:
        model = Dog
        fields = ('id', 'name', 'owner', 'birth_date')


class CatSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()


    class Meta:
        model = Cat
        fields = ('id', 'name', 'owner', 'birth_date')